/*************************************************************************/
/* Copyright (c) 2023 David Dehaene                                      */
/*                                                                       */
/* Permission is hereby granted, free of charge, to any person obtaining */
/* a copy of this software and associated documentation files (the       */
/* "Software"), to deal in the Software without restriction, including   */
/* without limitation the rights to use, copy, modify, merge, publish,   */
/* distribute, sublicense, and/or sell copies of the Software, and to    */
/* permit persons to whom the Software is furnished to do so, subject to */
/* the following conditions:                                             */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/

#ifndef SG_PINJOINT2D_INTERNAL_H
#define SG_PINJOINT2D_INTERNAL_H

#include "sg_fixed_vector2_internal.h"
#include "sg_bodies_2d_internal.h"

class SGPinJoint2DInternal {

	SGBody2DInternal* A;
	SGBody2DInternal* B;
	SGFixedVector2Internal anchor_A, anchor_B;
	SGFixedVector2Internal bias;
	SGFixedVector2Internal rA, rB;
	SGFixedTransform2DInternal M;
	SGFixedVector2Internal P;
	fixed softness;

	SGFixedVector2Internal custom_cross(const SGFixedVector2Internal& p_vec, fixed p_other);

public:
	void setup(fixed p_step);
	void solve(fixed p_step);
	void add_or_update(SGBody2DInternal* p_A, SGBody2DInternal* p_B, const SGFixedVector2Internal& p_pos);
	void remove();
	void set_bias(fixed p_bias);
	fixed get_bias() const;
	void set_softness(fixed p_softness);
	fixed get_softness() const;

	SGPinJoint2DInternal();
	~SGPinJoint2DInternal();

};


#endif