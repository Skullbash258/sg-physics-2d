/*************************************************************************/
/* Copyright (c) 2023 David Dehaene                                      */
/*                                                                       */
/* Permission is hereby granted, free of charge, to any person obtaining */
/* a copy of this software and associated documentation files (the       */
/* "Software"), to deal in the Software without restriction, including   */
/* without limitation the rights to use, copy, modify, merge, publish,   */
/* distribute, sublicense, and/or sell copies of the Software, and to    */
/* permit persons to whom the Software is furnished to do so, subject to */
/* the following conditions:                                             */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/

#include "sg_contact_internal.h"


void SGContactInternal::setup(fixed p_step,
	const SGFixedVector2Internal& p_normal, const SGFixedVector2Internal* p_contact_points) {
	fixed param_bias = fixed(19660); // 0.3
	fixed inv_dt = fixed::ONE / p_step;

	normal = p_normal;
	depth = normal.dot(p_contact_points[1] - p_contact_points[0]);
	rA = p_contact_points[0] - A->get_transform().get_origin();
	rB = p_contact_points[1] - B->get_transform().get_origin();

	fixed rA_normal = rA.dot(normal);
	fixed rB_normal = rB.dot(normal);
	fixed sum_inv_mass = A->get_inv_mass() + B->get_inv_mass();
	fixed inv_mass_normal = sum_inv_mass + A->get_inv_inertia() * (rA.length_squared() - rA_normal * rA_normal) + B->get_inv_inertia() * (rB.length_squared() - rB_normal * rB_normal);
	mass_normal = fixed::ONE / inv_mass_normal;

	SGFixedVector2Internal tangent = normal.tangent();
	fixed rA_tangent = rA.dot(tangent);
	fixed rB_tangent = rB.dot(tangent);
	fixed inv_mass_tangent = sum_inv_mass + A->get_inv_inertia() * (rA.length_squared() - rA_tangent * rA_tangent) + B->get_inv_inertia() * (rB.length_squared() - rB_tangent * rB_tangent);
	mass_tangent = fixed::ONE / inv_mass_tangent;

	fixed max_penetration = fixed(19660); // 0.3
	bias = -param_bias * inv_dt * MIN(fixed(0), -depth + max_penetration);

	fixed combined_bounce = CLAMP(A->get_bounce() + B->get_bounce(), fixed::ZERO, fixed::ONE);
	if (combined_bounce > fixed::ZERO) {
		SGFixedVector2Internal contact_velocity_A = A->get_velocity() + SGFixedVector2Internal(-A->get_angular_velocity() * rA.y, A->get_angular_velocity() * rA.x);
		SGFixedVector2Internal contact_velocity_B = B->get_velocity() + SGFixedVector2Internal(-B->get_angular_velocity() * rB.y, B->get_angular_velocity() * rB.x);
		SGFixedVector2Internal contact_relative_velocity = contact_velocity_B - contact_velocity_A;
		fixed normal_velocity = contact_relative_velocity.dot(normal);
		bounce = combined_bounce * normal_velocity;
	}

	// print_line(vformat("normal = (%s, %s), depth = %s, mass_normal = %s, mass_tangent = %s", normal.x.to_float(), normal.y.to_float(), depth.to_float(), mass_normal.to_float(), mass_tangent.to_float()));
}

void SGContactInternal::apply_impulse(const SGFixedVector2Internal& impulse) {
	A->apply_impulse(rA, -impulse);
	B->apply_impulse(rB, impulse);
}

void SGContactInternal::apply_bias_impulse(const SGFixedVector2Internal& bias_impulse) {
	A->apply_bias_impulse(rA, -bias_impulse);
	B->apply_bias_impulse(rB, bias_impulse);
}

void SGContactInternal::solve(fixed p_step) {
	SGFixedVector2Internal contact_velocity_A = A->get_velocity() + SGFixedVector2Internal(-A->get_angular_velocity() * rA.y, A->get_angular_velocity() * rA.x);
	SGFixedVector2Internal contact_velocity_B = B->get_velocity() + SGFixedVector2Internal(-B->get_angular_velocity() * rB.y, B->get_angular_velocity() * rB.x);
	SGFixedVector2Internal contact_relative_velocity = contact_velocity_B - contact_velocity_A;
	// print_line(vformat("velocities: A = (%s, %s), B = (%s, %s)", contact_velocity_A.x.to_float(), contact_velocity_A.y.to_float(), contact_velocity_B.x.to_float(), contact_velocity_B.y.to_float()));
	fixed normal_velocity = contact_relative_velocity.dot(normal);

	SGFixedVector2Internal bias_contact_velocity_A = A->get_bias_velocity() + SGFixedVector2Internal(-A->get_bias_angular_velocity() * rA.y, A->get_bias_angular_velocity() * rA.x);
	SGFixedVector2Internal bias_contact_velocity_B = B->get_bias_velocity() + SGFixedVector2Internal(-B->get_bias_angular_velocity() * rB.y, B->get_bias_angular_velocity() * rB.x);
	SGFixedVector2Internal bias_contact_relative_velocity = bias_contact_velocity_B - bias_contact_velocity_A;

	fixed bias_normal_velocity = bias_contact_relative_velocity.dot(normal);
	SGFixedVector2Internal tangent = normal.tangent();
	fixed tangent_velocity = contact_relative_velocity.dot(tangent);
	// print_line(vformat("normal_velocity = %s, tangent_velocity = %s", normal_velocity.to_float(), tangent_velocity.to_float()));

	fixed bias_impulse_normal = (bias + bias_normal_velocity) * mass_normal; //inverse
	fixed previous_bias_impulse_normal = accumulated_bias_impulse;
	accumulated_bias_impulse = MAX(previous_bias_impulse_normal + bias_impulse_normal, fixed(0));

	SGFixedVector2Internal bias_impulse = -normal * (accumulated_bias_impulse - previous_bias_impulse_normal); //inverse
	apply_bias_impulse(bias_impulse);

	fixed impulse_normal = -(bounce + normal_velocity) * mass_normal;
	fixed previous_impulse_normal = accumulated_normal_impulse;
	accumulated_normal_impulse = MIN(previous_impulse_normal + impulse_normal, fixed(0)); //inverse

	fixed friction = MIN(A->get_friction(), B->get_friction());
	fixed impulse_tangent_max = -friction * accumulated_normal_impulse; //inverse
	fixed impulse_tangent = -tangent_velocity * mass_tangent;
	fixed previous_impulse_tangent = accumulated_tangent_impulse;
	accumulated_tangent_impulse = CLAMP(previous_impulse_tangent + impulse_tangent, -impulse_tangent_max, impulse_tangent_max);

	SGFixedVector2Internal impulse = normal * (accumulated_normal_impulse - previous_impulse_normal) + tangent * (accumulated_tangent_impulse - previous_impulse_tangent);
	apply_impulse(impulse);
}

SGContactInternal::SGContactInternal(fixed p_step, SGBody2DInternal* p_A, SGBody2DInternal* p_B,
	SGShape2DInternal* p_shape_A, SGShape2DInternal* p_shape_B,
	const SGFixedVector2Internal& p_normal, const SGFixedVector2Internal* p_contact_points)
	: A(p_A), B(p_B), shape_A(p_shape_A), shape_B(p_shape_B) {
	setup(p_step, p_normal, p_contact_points);
}

SGContactInternal::~SGContactInternal() {

}