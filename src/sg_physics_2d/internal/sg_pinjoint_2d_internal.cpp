/*************************************************************************/
/* Copyright (c) 2023 David Dehaene                                      */
/*                                                                       */
/* Permission is hereby granted, free of charge, to any person obtaining */
/* a copy of this software and associated documentation files (the       */
/* "Software"), to deal in the Software without restriction, including   */
/* without limitation the rights to use, copy, modify, merge, publish,   */
/* distribute, sublicense, and/or sell copies of the Software, and to    */
/* permit persons to whom the Software is furnished to do so, subject to */
/* the following conditions:                                             */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/

#include "sg_pinjoint_2d_internal.h"

#include "sg_world_2d_internal.h"


void SGPinJoint2DInternal::setup(fixed p_step) {
	rA = A->get_transform().basis_xform(anchor_A);
	rB = B->get_transform().basis_xform(anchor_B);

	SGFixedTransform2DInternal K1;
	K1[0].x = A->get_inv_mass() + B->get_inv_mass();
	K1[1].x = fixed(0);
	K1[0].y = fixed(0);
	K1[1].y = A->get_inv_mass() + B->get_inv_mass();

	SGFixedTransform2DInternal K2;
	K2[0].x = A->get_inv_inertia() * rA.y * rA.y;
	K2[1].x = -A->get_inv_inertia() * rA.x * rA.y;
	K2[0].y = -A->get_inv_inertia() * rA.x * rA.y;
	K2[1].y = A->get_inv_inertia() * rA.x * rA.x;

	SGFixedTransform2DInternal K;
	K[0] = K1[0] + K2[0];
	K[1] = K1[1] + K2[1];

	SGFixedTransform2DInternal K3;
	K3[0].x = B->get_inv_inertia() * rB.y * rB.y;
	K3[1].x = -B->get_inv_inertia() * rB.x * rB.y;
	K3[0].y = -B->get_inv_inertia() * rB.x * rB.y;
	K3[1].y = B->get_inv_inertia() * rB.x * rB.x;

	K[0] += K3[0];
	K[1] += K3[1];

	K[0].x += softness;
	K[1].y += softness;

	M = K.affine_inverse();

	SGFixedVector2Internal gA = rA + A->get_transform().get_origin();
	SGFixedVector2Internal gB = rB + B->get_transform().get_origin();

	SGFixedVector2Internal delta = gB - gA;

	bias = delta * -bias / p_step;
}


void SGPinJoint2DInternal::add_or_update(SGBody2DInternal* p_A, SGBody2DInternal* p_B, const SGFixedVector2Internal& p_pos) {
	A = p_A;
	B = p_B;
	anchor_A = A->get_transform().inverse().xform(p_pos);
	anchor_B = B->get_transform().inverse().xform(p_pos);
}


void SGPinJoint2DInternal::solve(fixed p_step) {
	// compute relative velocity
	SGFixedVector2Internal vA = A->get_velocity() - custom_cross(rA, A->get_angular_velocity());

	SGFixedVector2Internal rel_vel;
	rel_vel = B->get_velocity() - custom_cross(rB, B->get_angular_velocity()) - vA;

	SGFixedVector2Internal impulse = M.basis_xform(bias - rel_vel - SGFixedVector2Internal(softness, softness) * P);

	A->apply_impulse(rA, -impulse);
	B->apply_impulse(rB, impulse);

	P += impulse;
}


void SGPinJoint2DInternal::set_bias(fixed p_bias) {
	bias = SGFixedVector2Internal(p_bias, p_bias);
}


fixed SGPinJoint2DInternal::get_bias() const {
	return bias.x;
}


void SGPinJoint2DInternal::set_softness(fixed p_softness) {
	softness = p_softness;
}


fixed SGPinJoint2DInternal::get_softness() const {
	return softness;
}


SGFixedVector2Internal SGPinJoint2DInternal::custom_cross(const SGFixedVector2Internal& p_vec, fixed p_other) {
	return SGFixedVector2Internal(p_other * p_vec.y, -p_other * p_vec.x);
}


SGPinJoint2DInternal::SGPinJoint2DInternal() {

}

SGPinJoint2DInternal::~SGPinJoint2DInternal() {

}