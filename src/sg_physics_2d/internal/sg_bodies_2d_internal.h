/*************************************************************************/
/* Copyright (c) 2021-2022 David Snopek                                  */
/*                                                                       */
/* Permission is hereby granted, free of charge, to any person obtaining */
/* a copy of this software and associated documentation files (the       */
/* "Software"), to deal in the Software without restriction, including   */
/* without limitation the rights to use, copy, modify, merge, publish,   */
/* distribute, sublicense, and/or sell copies of the Software, and to    */
/* permit persons to whom the Software is furnished to do so, subject to */
/* the following conditions:                                             */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/

#ifndef SG_BODIES_2D_INTERNAL_H
#define SG_BODIES_2D_INTERNAL_H

#include <vector>

#include "sg_shapes_2d_internal.h"
#include "sg_broadphase_2d_internal.h"

class SGWorld2DInternal;

class SGCollisionObject2DInternal {
public:
	enum ObjectType {
		OBJECT_AREA = 1,
		OBJECT_BODY = 2,
		OBJECT_BOTH = (OBJECT_AREA | OBJECT_BODY),
	};

private:
	ObjectType object_type;
	SGFixedTransform2DInternal transform;
	std::vector<SGShape2DInternal *> shapes;
	SGWorld2DInternal *world;
	SGBroadphase2DInternal *broadphase;
	SGBroadphase2DInternal::Element *broadphase_element;
	void *data;

	uint32_t collision_layer;
	uint32_t collision_mask;
	bool monitorable;

	friend class SGWorld2DInternal;

	_FORCE_INLINE_ void set_world(SGWorld2DInternal *p_world) {
		world = p_world;
	}

public:
	_FORCE_INLINE_ ObjectType get_object_type() const { return object_type; }
	_FORCE_INLINE_ SGWorld2DInternal *get_world() const { return world; }

	_FORCE_INLINE_ SGFixedTransform2DInternal get_transform() const { return transform; }
	void set_transform(const SGFixedTransform2DInternal &p_transform);

	void add_shape(SGShape2DInternal *p_shape);
	void remove_shape(SGShape2DInternal *p_shape);

	_FORCE_INLINE_ const std::vector<SGShape2DInternal *> &get_shapes() const {
		return shapes;
	}

	SGFixedRect2Internal get_bounds() const;

	void add_to_broadphase(SGBroadphase2DInternal *p_broadphase);
	void remove_from_broadphase();

	_FORCE_INLINE_ void set_data(void *p_data) { data = p_data; }
	_FORCE_INLINE_ void *get_data() const { return data; }

	_FORCE_INLINE_ void set_collision_layer(uint32_t p_collision_layer) { collision_layer = p_collision_layer; }
	_FORCE_INLINE_ uint32_t get_collision_layer() const { return collision_layer; }

	_FORCE_INLINE_ void set_collision_mask(uint32_t p_collision_mask) { collision_mask = p_collision_mask; }
	_FORCE_INLINE_ uint32_t get_collision_mask() const { return collision_mask; }

	void set_monitorable(bool p_monitorable);
	_FORCE_INLINE_ bool get_monitorable() { return monitorable; }

	_FORCE_INLINE_ bool test_collision_layers(SGCollisionObject2DInternal *p_other) const {
		return (collision_layer & p_other->collision_mask) || (p_other->collision_layer & collision_mask);
	}

	SGCollisionObject2DInternal(ObjectType p_type);
	virtual ~SGCollisionObject2DInternal();

};

class SGArea2DInternal : public SGCollisionObject2DInternal {
public:
	SGArea2DInternal();
	~SGArea2DInternal();
};

class SGBody2DInternal : public SGCollisionObject2DInternal {
public:

	enum BodyType {
		BODY_STATIC = 1,
		BODY_KINEMATIC = 2,
		BODY_RIGID = 3,
	};

protected:
	BodyType body_type;
	fixed safe_margin;
	fixed friction = fixed::ONE;
	fixed bounce = fixed::ZERO;

public:
	_FORCE_INLINE_ BodyType get_body_type() const { return body_type; }

	virtual fixed get_inv_mass() const { return fixed(0); }
	virtual fixed get_inv_inertia() const { return fixed(0); }
	virtual void apply_impulse(const SGFixedVector2Internal& p_offset, const SGFixedVector2Internal& p_impulse) {}
	virtual void apply_bias_impulse(const SGFixedVector2Internal& p_offset, const SGFixedVector2Internal& p_bias_impulse) {}
	virtual SGFixedVector2Internal get_velocity() const { return SGFixedVector2Internal(); }
	virtual fixed get_angular_velocity() const { return fixed(0); }
	virtual SGFixedVector2Internal get_bias_velocity() const { return SGFixedVector2Internal(); }
	virtual fixed get_bias_angular_velocity() const { return fixed(0); }
	void set_friction(fixed p_friction) { friction = p_friction; }
	fixed get_friction() { return friction; }
	void set_bounce(fixed p_bounce) { bounce = p_bounce; }
	fixed get_bounce() { return bounce; }

	void set_safe_margin(fixed p_safe_margin) { safe_margin = p_safe_margin; }
	_FORCE_INLINE_ fixed get_safe_margin() const { return safe_margin; }

	SGBody2DInternal(BodyType p_type);
	~SGBody2DInternal();
};

class SGStaticBody2DInternal : public SGBody2DInternal {
public:
	SGStaticBody2DInternal();
	~SGStaticBody2DInternal();
};

class SGKinematicBody2DInternal : public SGBody2DInternal {
public:
	SGKinematicBody2DInternal();
	~SGKinematicBody2DInternal();
};

class SGRigidBody2DInternal : public SGBody2DInternal {
	fixed mass = fixed(655360);
	fixed inv_mass = fixed(6553);
	fixed inertia = fixed(6553600);
	fixed inv_inertia = fixed(655);
	fixed damp = fixed(6553);
	fixed angular_damp = fixed(65536);
	fixed gravity_scale = fixed(65536);

	SGFixedVector2Internal velocity = SGFixedVector2Internal(fixed(0), fixed(0));
	fixed angular_velocity = fixed(0);
	SGFixedVector2Internal bias_velocity = SGFixedVector2Internal(fixed(0), fixed(0));
	fixed bias_angular_velocity = fixed(0);

public:
	SGRigidBody2DInternal();
	~SGRigidBody2DInternal();

	virtual void apply_impulse(const SGFixedVector2Internal& p_offset, const SGFixedVector2Internal& p_impulse) override;
	virtual void apply_bias_impulse(const SGFixedVector2Internal& p_offset, const SGFixedVector2Internal& p_bias_impulse) override;
	void integrate_forces(fixed p_step);
	void integrate_velocities(fixed p_step);
	void set_inertia(fixed p_inertia);
	fixed get_inertia() const;
	void set_mass(fixed p_mass);
	fixed get_mass() const;
	virtual fixed get_inv_mass() const override { return inv_mass; };
	virtual fixed get_inv_inertia() const override { return inv_inertia; };
	virtual SGFixedVector2Internal get_velocity() const override { return velocity; }
	virtual fixed get_angular_velocity() const override { return angular_velocity; }
	virtual SGFixedVector2Internal get_bias_velocity() const override { return bias_velocity; }
	virtual fixed get_bias_angular_velocity() const override { return bias_angular_velocity; }
	void set_damp(fixed p_damp) { damp = p_damp; }
	fixed get_damp() { return damp; }
	void set_angular_damp(fixed p_angular_damp) { angular_damp = p_angular_damp; }
	fixed get_angular_damp() { return angular_damp; }
	void set_gravity_scale(fixed p_gravity_scale) { gravity_scale = p_gravity_scale; }
	fixed get_gravity_scale() { return gravity_scale; }
	void set_velocity(SGFixedVector2Internal p_velocity) { velocity = p_velocity; }
	SGFixedVector2Internal get_velocity() { return velocity; }
	void set_angular_velocity(fixed p_angular_velocity) { angular_velocity = p_angular_velocity; }
	fixed get_angular_velocity() { return angular_velocity; }
	void set_bias_angular_velocity(fixed p_bias_angular_velocity) { bias_angular_velocity = p_bias_angular_velocity; }
	void set_bias_velocity(SGFixedVector2Internal p_bias_velocity) { bias_velocity = p_bias_velocity; }

};


#endif
