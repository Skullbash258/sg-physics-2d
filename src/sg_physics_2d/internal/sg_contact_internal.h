/*************************************************************************/
/* Copyright (c) 2023 David Dehaene                                      */
/*                                                                       */
/* Permission is hereby granted, free of charge, to any person obtaining */
/* a copy of this software and associated documentation files (the       */
/* "Software"), to deal in the Software without restriction, including   */
/* without limitation the rights to use, copy, modify, merge, publish,   */
/* distribute, sublicense, and/or sell copies of the Software, and to    */
/* permit persons to whom the Software is furnished to do so, subject to */
/* the following conditions:                                             */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/

#ifndef SG_CONTACT_INTERNAL_H
#define SG_CONTACT_INTERNAL_H

#include "sg_fixed_vector2_internal.h"
#include "sg_bodies_2d_internal.h"

struct SGContactInternal {
	SGBody2DInternal* A;
	SGBody2DInternal* B;
	SGShape2DInternal* shape_A;
	SGShape2DInternal* shape_B;
	SGFixedVector2Internal normal;
	SGFixedVector2Internal local_A, local_B;
	fixed accumulated_normal_impulse = fixed(0);
	fixed accumulated_tangent_impulse = fixed(0);
	fixed accumulated_bias_impulse = fixed(0);
	fixed mass_normal, mass_tangent;
	fixed bias;

	fixed depth;
	bool active;
	SGFixedVector2Internal rA, rB;
	bool reused;
	fixed bounce = fixed(0);

	void setup(fixed p_step,
		const SGFixedVector2Internal& p_normal, const SGFixedVector2Internal* p_contact_points);
	void solve(fixed p_step);

	SGContactInternal(fixed p_step, SGBody2DInternal* p_A, SGBody2DInternal* p_B,
		SGShape2DInternal* p_shape_A, SGShape2DInternal* p_shape_B,
		const SGFixedVector2Internal& p_normal, const SGFixedVector2Internal* p_contact_points);
	~SGContactInternal();

private:
	void apply_impulse(const SGFixedVector2Internal& impulse);
	void apply_bias_impulse(const SGFixedVector2Internal& bias_impulse);
};


#endif