/*************************************************************************/
/* Copyright (c) 2023 David Dehaene                                      */
/*                                                                       */
/* Permission is hereby granted, free of charge, to any person obtaining */
/* a copy of this software and associated documentation files (the       */
/* "Software"), to deal in the Software without restriction, including   */
/* without limitation the rights to use, copy, modify, merge, publish,   */
/* distribute, sublicense, and/or sell copies of the Software, and to    */
/* permit persons to whom the Software is furnished to do so, subject to */
/* the following conditions:                                             */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/

#ifndef SG_PHYSICS_SINGLETON_H
#define SG_PHYSICS_SINGLETON_H

//#include <core/object.h>
//#include <scene/main/scene_tree.h>
//#include "scene/main/node.h"


#include "../../../internal/sg_fixed_number_internal.h"
#include "../../../internal/sg_world_2d_internal.h"
#include "sg_rigid_body_2d.h"

//TODO: Merge this into the Physics Server

/*
* 
I know you're branched from before the SGPhysics2DServer existed, 
but for when this is merged upstream, 
I'd prefer for this to be merged into SGPhysics2DServer.

*/


/*
class SGPhysics : public Object {

	GDCLASS(SGPhysics, Object);

	static SGPhysics* singleton;

protected:
	static void _bind_methods();

public:
	static SGPhysics* get_singleton();
	void step(Object* p_tree, int64_t p_step, int64_t count);
	Dictionary save_internals(Object* p_tree);
	void load_internals(Object* p_tree, Dictionary d);

	SGPhysics();
	~SGPhysics();
};

*/



#endif