/*************************************************************************/
/* Copyright (c) 2023 David Dehaene                                      */
/*                                                                       */
/* Permission is hereby granted, free of charge, to any person obtaining */
/* a copy of this software and associated documentation files (the       */
/* "Software"), to deal in the Software without restriction, including   */
/* without limitation the rights to use, copy, modify, merge, publish,   */
/* distribute, sublicense, and/or sell copies of the Software, and to    */
/* permit persons to whom the Software is furnished to do so, subject to */
/* the following conditions:                                             */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/

#ifndef SG_PINJOINT_2D_H
#define SG_PINJOINT_2D_H

#include "sg_fixed_node_2d.h"

#include "sg_rigid_body_2d.h"
#include "../../../internal/sg_pinjoint_2d_internal.h"
#include "../../../internal/sg_world_2d_internal.h"

class SGPinJoint2D : public SGFixedNode2D {
	GDCLASS(SGPinJoint2D, SGFixedNode2D);

	SGPinJoint2DInternal* internal;

	NodePath a;
	NodePath b;

protected:
	static void _bind_methods();

public:
	bool is_valid = false;

	SGPinJoint2DInternal* get_internal() { return internal; };

	void set_node_a(const NodePath& node_a);
	NodePath get_node_a() const;

	void set_node_b(const NodePath& node_b);
	NodePath get_node_b() const;

	void set_bias(int64_t p_bias);
	int64_t get_bias() const;

	void set_softness(int64_t p_softness);
	int64_t get_softness() const;

	void update_internal();

	SGPinJoint2D();
	~SGPinJoint2D();

};

#endif