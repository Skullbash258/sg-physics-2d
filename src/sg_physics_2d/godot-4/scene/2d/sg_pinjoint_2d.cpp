/*************************************************************************/
/* Copyright (c) 2023 David Dehaene                                      */
/*                                                                       */
/* Permission is hereby granted, free of charge, to any person obtaining */
/* a copy of this software and associated documentation files (the       */
/* "Software"), to deal in the Software without restriction, including   */
/* without limitation the rights to use, copy, modify, merge, publish,   */
/* distribute, sublicense, and/or sell copies of the Software, and to    */
/* permit persons to whom the Software is furnished to do so, subject to */
/* the following conditions:                                             */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/

#include "sg_pinjoint_2d.h"

void SGPinJoint2D::_bind_methods() {
	ClassDB::bind_method(D_METHOD("set_node_a", "node_a"), &SGPinJoint2D::set_node_a);
	ClassDB::bind_method(D_METHOD("get_node_a"), &SGPinJoint2D::get_node_a);
	ClassDB::bind_method(D_METHOD("set_node_b", "node_b"), &SGPinJoint2D::set_node_b);
	ClassDB::bind_method(D_METHOD("get_node_b"), &SGPinJoint2D::get_node_b);
	ClassDB::bind_method(D_METHOD("set_bias", "bias"), &SGPinJoint2D::set_bias);
	ClassDB::bind_method(D_METHOD("get_bias"), &SGPinJoint2D::get_bias);
	ClassDB::bind_method(D_METHOD("set_softness", "softness"), &SGPinJoint2D::set_softness);
	ClassDB::bind_method(D_METHOD("get_softness"), &SGPinJoint2D::get_softness);

	ADD_PROPERTY(PropertyInfo(Variant::INT, "bias"), "set_bias", "get_bias");
	ADD_PROPERTY(PropertyInfo(Variant::INT, "softness"), "set_softness", "get_softness");
	ADD_PROPERTY(PropertyInfo(Variant::NODE_PATH, "node_a"), "set_node_a", "get_node_a");
	ADD_PROPERTY(PropertyInfo(Variant::NODE_PATH, "node_b"), "set_node_b", "get_node_b");
}

void SGPinJoint2D::update_internal() {
	Node* node_a = get_node_or_null(a);
	Node* node_b = get_node_or_null(b);

	SGRigidBody2D* body_a = Object::cast_to<SGRigidBody2D>(node_a);
	SGRigidBody2D* body_b = Object::cast_to<SGRigidBody2D>(node_b);

	if (!body_a || !body_b) {
		is_valid = false;
		return;
	}

	body_a->sync_to_physics_engine();
	body_b->sync_to_physics_engine();
	internal->add_or_update(body_a->get_internal(), body_b->get_internal(), get_global_fixed_transform_internal().get_origin());
	is_valid = true;
}

void SGPinJoint2D::set_node_a(const NodePath& node_a) {
	if (a == node_a) {
		return;
	}

	a = node_a;
	update_internal();
}


NodePath SGPinJoint2D::get_node_a() const {
	return a;
}


void SGPinJoint2D::set_node_b(const NodePath& node_b) {
	if (b == node_b) {
		return;
	}

	b = node_b;
	update_internal();
}


NodePath SGPinJoint2D::get_node_b() const {
	return b;
}


void SGPinJoint2D::set_bias(int64_t p_bias) {
	internal->set_bias(fixed(p_bias));
}


int64_t SGPinJoint2D::get_bias() const {
	return internal->get_bias().value;
}


void SGPinJoint2D::set_softness(int64_t p_softness) {
	internal->set_softness(fixed(p_softness));
}


int64_t SGPinJoint2D::get_softness() const {
	return internal->get_softness().value;
}


SGPinJoint2D::SGPinJoint2D() {
	internal = memnew(SGPinJoint2DInternal());

	add_to_group("SGPINJOINT");
}


SGPinJoint2D::~SGPinJoint2D() {
	memdelete(internal);
}
