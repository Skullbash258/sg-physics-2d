/*************************************************************************/
/* Copyright (c) 2021-2022 David Snopek                                  */
/*                                                                       */
/* Permission is hereby granted, free of charge, to any person obtaining */
/* a copy of this software and associated documentation files (the       */
/* "Software"), to deal in the Software without restriction, including   */
/* without limitation the rights to use, copy, modify, merge, publish,   */
/* distribute, sublicense, and/or sell copies of the Software, and to    */
/* permit persons to whom the Software is furnished to do so, subject to */
/* the following conditions:                                             */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/

#include "sg_static_body_2d.h"

#include "../../servers/sg_physics_2d_server.h"

void SGStaticBody2D::_bind_methods() {
	ClassDB::bind_method(D_METHOD("get_friction"), &SGStaticBody2D::get_friction);
	ClassDB::bind_method(D_METHOD("set_friction", "p_friction"), &SGStaticBody2D::set_friction);
	ClassDB::bind_method(D_METHOD("get_bounce"), &SGStaticBody2D::get_bounce);
	ClassDB::bind_method(D_METHOD("set_bounce", "p_bounce"), &SGStaticBody2D::set_bounce);

	ADD_PROPERTY(PropertyInfo(Variant::INT, "friction"), "set_friction", "get_friction");
	ADD_PROPERTY(PropertyInfo(Variant::INT, "bounce"), "set_bounce", "get_bounce");
}

void SGStaticBody2D::set_friction(int64_t p_friction) {
	get_internal()->set_friction(fixed(p_friction));
}

int64_t SGStaticBody2D::get_friction() {
	return get_internal()->get_friction().value;
}

void SGStaticBody2D::set_bounce(int64_t p_bounce) {
	get_internal()->set_bounce(fixed(p_bounce));
}

int64_t SGStaticBody2D::get_bounce() {
	return get_internal()->get_bounce().value;
}

SGStaticBody2D::SGStaticBody2D()
	: SGPhysicsBody2D(SGPhysics2DServer::get_singleton()->collision_object_create(SGPhysics2DServer::OBJECT_BODY, SGPhysics2DServer::BODY_STATIC))
{
}

SGStaticBody2D::~SGStaticBody2D() {
}
