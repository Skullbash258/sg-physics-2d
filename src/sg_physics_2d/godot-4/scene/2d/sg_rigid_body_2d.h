/*************************************************************************/
/* Copyright (c) 2023 David Dehaene                                      */
/*                                                                       */
/* Permission is hereby granted, free of charge, to any person obtaining */
/* a copy of this software and associated documentation files (the       */
/* "Software"), to deal in the Software without restriction, including   */
/* without limitation the rights to use, copy, modify, merge, publish,   */
/* distribute, sublicense, and/or sell copies of the Software, and to    */
/* permit persons to whom the Software is furnished to do so, subject to */
/* the following conditions:                                             */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/

#ifndef SG_RIGID_BODY_2D_H
#define SG_RIGID_BODY_2D_H

#include "sg_physics_body_2d.h"
#include "sg_collision_object_2d.h"
#include "../../../internal/sg_bodies_2d_internal.h"
#include "../../../internal/sg_world_2d_internal.h"

class SGRigidBody2D : public SGPhysicsBody2D {
	GDCLASS(SGRigidBody2D, SGPhysicsBody2D);

protected:
	static void _bind_methods();

public:
	void integrate_forces(fixed p_step);

	_FORCE_INLINE_ SGRigidBody2DInternal* get_internal() const { return (SGRigidBody2DInternal*)SGCollisionObject2D::get_internal(); }

	void apply_impulse(const Ref<SGFixedVector2>& p_offset, const Ref<SGFixedVector2>& p_impulse);
	Array get_contacts() const;
	void set_mass(int64_t p_mass);
	int64_t get_mass();
	void set_inertia(int64_t p_inertia);
	int64_t get_inertia();
	void set_damp(int64_t p_damp);
	int64_t get_damp();
	void set_angular_damp(int64_t p_angular_damp);
	int64_t get_angular_damp();
	void set_gravity_scale(int64_t p_gravity_scale);
	int64_t get_gravity_scale();
	void set_velocity(const Ref<SGFixedVector2>& p_velocity);
	Ref<SGFixedVector2> get_velocity();
	void set_angular_velocity(int64_t p_angular_velocity);
	int64_t get_angular_velocity();
	void set_friction(int64_t p_friction);
	int64_t get_friction();
	void set_bounce(int64_t p_bounce);
	int64_t get_bounce();
	Dictionary save_internals();
	void load_internals(Dictionary d);

	SGRigidBody2D();
	~SGRigidBody2D();

};

#endif