/*************************************************************************/
/* Copyright (c) 2023 David Dehaene                                      */
/*                                                                       */
/* Permission is hereby granted, free of charge, to any person obtaining */
/* a copy of this software and associated documentation files (the       */
/* "Software"), to deal in the Software without restriction, including   */
/* without limitation the rights to use, copy, modify, merge, publish,   */
/* distribute, sublicense, and/or sell copies of the Software, and to    */
/* permit persons to whom the Software is furnished to do so, subject to */
/* the following conditions:                                             */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/

#include "sg_physics_singleton.h"

#include "sg_pinjoint_2d.h"

// TODO: Resolve Comment

/*

Using groups is a clever way to get nodes in scene tree order!
I'm not so sure about baking that into these methods, though. 
In later versions (which have SGPhysics2DServer) you can put sets of physics objects into different "worlds", and you may want to step each world separately.
Maybe have the developer pass in their own lists of objects? 
They can use groups get them in a predictable order. 
Or, maybe the objects could somehow be stored on the world in order? 
We really only need to re-order them when a new node is added.

*/


/*
SGPhysics* SGPhysics::singleton = NULL;

SGPhysics::SGPhysics() {
	ERR_FAIL_COND(singleton != NULL);
	singleton = this;
}

SGPhysics::~SGPhysics() {
	singleton = NULL;
}

void SGPhysics::_bind_methods() {
	//ClassDB::bind_method(D_METHOD("step", "tree", "p_step", "count"), &SGPhysics::step);
	//ClassDB::bind_method(D_METHOD("save_internals", "tree"), &SGPhysics::save_internals);
	//ClassDB::bind_method(D_METHOD("load_internals", "tree", "state"), &SGPhysics::load_internals);
}

void SGPhysics::step(Object* p_tree, int64_t p_step, int64_t count) {
	SceneTree* tree = Object::cast_to<SceneTree>(p_tree);
	ERR_FAIL_COND_V(!tree, );
	List<SGRigidBody2D*> rigid_bodies;
	List<SGRigidBody2DInternal*> rigid_body_internals;
	List<SGPinJoint2DInternal*> pinjoints;

	{
		List<Node*> nodes;
		tree->get_nodes_in_group("SGRIGIDBODY", &nodes);

		for (List<Node*>::Element* E = nodes.front(); E; E = E->next()) {
			Node* node = E->get();

			SGRigidBody2D* rigid_body = Object::cast_to<SGRigidBody2D>(node);
			if (rigid_body) {
				rigid_bodies.push_back(rigid_body);
				rigid_body_internals.push_back(rigid_body->get_internal());
			}
		}
	}

	{
		List<Node*> nodes;
		tree->get_nodes_in_group("SGPINJOINT", &nodes);


		for (List<Node*>::Element* E = nodes.front(); E; E = E->next()) {
			Node* node = E->get();

			SGPinJoint2D* pinjoint = Object::cast_to<SGPinJoint2D>(node);
			if (pinjoint && pinjoint->is_valid) {
				pinjoints.push_back(pinjoint->get_internal());
			}
		}
	}

	SGWorld2DInternal::get_singleton()->step(rigid_body_internals, pinjoints, fixed(p_step), count);

	for (List<SGRigidBody2D*>::Element* E = rigid_bodies.front(); E; E = E->next()) {
		SGRigidBody2D* rigid_body = E->get();

		rigid_body->sync_from_physics_engine();
	}
}


Dictionary SGPhysics::save_internals(Object* p_tree) {
	SceneTree* tree = Object::cast_to<SceneTree>(p_tree);
	Dictionary state;
	List<Node*> nodes;
	tree->get_nodes_in_group("SGRIGIDBODY", &nodes);

	for (List<Node*>::Element* E = nodes.front(); E; E = E->next()) {
		Node* node = E->get();

		SGRigidBody2D* rigid_body = Object::cast_to<SGRigidBody2D>(node);
		if (rigid_body && rigid_body->is_inside_tree() && !rigid_body->is_queued_for_deletion()) {
			NodePath path = node->get_path();
			if (path != NodePath()) {
				state[String(path)] = rigid_body->save_internals();
			}
		}
	}

	return state;
}

void SGPhysics::load_internals(Object* p_tree, Dictionary state) {
	SceneTree* tree = Object::cast_to<SceneTree>(p_tree);
	List<Node*> nodes;
	tree->get_nodes_in_group("SGRIGIDBODY", &nodes);

	for (List<Node*>::Element* E = nodes.front(); E; E = E->next()) {
		Node* node = E->get();

		SGRigidBody2D* rigid_body = Object::cast_to<SGRigidBody2D>(node);
		if (rigid_body && rigid_body->is_inside_tree() && !rigid_body->is_queued_for_deletion()) {
			String path(node->get_path());
			if (state.has(path)) {
				rigid_body->load_internals(state[path]);
			}
		}
	}
}


SGPhysics* SGPhysics::get_singleton() {
	return singleton;
}

*/