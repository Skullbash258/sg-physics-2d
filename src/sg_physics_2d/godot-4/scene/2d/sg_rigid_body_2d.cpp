/*************************************************************************/
/* Copyright (c) 2023 David Dehaene                                      */
/*                                                                       */
/* Permission is hereby granted, free of charge, to any person obtaining */
/* a copy of this software and associated documentation files (the       */
/* "Software"), to deal in the Software without restriction, including   */
/* without limitation the rights to use, copy, modify, merge, publish,   */
/* distribute, sublicense, and/or sell copies of the Software, and to    */
/* permit persons to whom the Software is furnished to do so, subject to */
/* the following conditions:                                             */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/

#include "sg_rigid_body_2d.h"

#include "../../servers/sg_physics_2d_server.h"
#include "../../../internal/sg_bodies_2d_internal.h"

void SGRigidBody2D::_bind_methods() {
	ClassDB::bind_method(D_METHOD("sync_from_physics_engine"), &SGRigidBody2D::sync_from_physics_engine);
	ClassDB::bind_method(D_METHOD("apply_impulse", "offset", "impulse"), &SGRigidBody2D::apply_impulse);
	ClassDB::bind_method(D_METHOD("get_contacts"), &SGRigidBody2D::get_contacts);
	ClassDB::bind_method(D_METHOD("get_mass"), &SGRigidBody2D::get_mass);
	ClassDB::bind_method(D_METHOD("set_mass", "p_mass"), &SGRigidBody2D::set_mass);
	ClassDB::bind_method(D_METHOD("get_inertia"), &SGRigidBody2D::get_inertia);
	ClassDB::bind_method(D_METHOD("set_inertia", "p_inertia"), &SGRigidBody2D::set_inertia);
	ClassDB::bind_method(D_METHOD("get_damp"), &SGRigidBody2D::get_damp);
	ClassDB::bind_method(D_METHOD("set_damp", "p_damp"), &SGRigidBody2D::set_damp);
	ClassDB::bind_method(D_METHOD("get_angular_damp"), &SGRigidBody2D::get_angular_damp);
	ClassDB::bind_method(D_METHOD("set_angular_damp", "p_angular_damp"), &SGRigidBody2D::set_angular_damp);
	ClassDB::bind_method(D_METHOD("get_velocity"), &SGRigidBody2D::get_velocity);
	ClassDB::bind_method(D_METHOD("set_velocity", "p_velocity"), &SGRigidBody2D::set_velocity);
	ClassDB::bind_method(D_METHOD("get_gravity_scale"), &SGRigidBody2D::get_gravity_scale);
	ClassDB::bind_method(D_METHOD("set_gravity_scale", "p_gravity_scale"), &SGRigidBody2D::set_gravity_scale);
	ClassDB::bind_method(D_METHOD("get_angular_velocity"), &SGRigidBody2D::get_angular_velocity);
	ClassDB::bind_method(D_METHOD("set_angular_velocity", "p_angular_velocity"), &SGRigidBody2D::set_angular_velocity);
	ClassDB::bind_method(D_METHOD("get_friction"), &SGRigidBody2D::get_friction);
	ClassDB::bind_method(D_METHOD("set_friction", "p_friction"), &SGRigidBody2D::set_friction);
	ClassDB::bind_method(D_METHOD("get_bounce"), &SGRigidBody2D::get_bounce);
	ClassDB::bind_method(D_METHOD("set_bounce", "p_bounce"), &SGRigidBody2D::set_bounce);

	ADD_PROPERTY(PropertyInfo(Variant::INT, "mass"), "set_mass", "get_mass");
	ADD_PROPERTY(PropertyInfo(Variant::INT, "inertia"), "set_inertia", "get_inertia");
	ADD_PROPERTY(PropertyInfo(Variant::INT, "damp"), "set_damp", "get_damp");
	ADD_PROPERTY(PropertyInfo(Variant::INT, "angular_damp"), "set_angular_damp", "get_angular_damp");
	ADD_PROPERTY(PropertyInfo(Variant::OBJECT, "velocity", PROPERTY_HINT_NONE, "", 0), "set_velocity", "get_velocity");
	ADD_PROPERTY(PropertyInfo(Variant::INT, "angular_velocity"), "set_angular_velocity", "get_angular_velocity");
	ADD_PROPERTY(PropertyInfo(Variant::INT, "gravity_scale"), "set_gravity_scale", "get_gravity_scale");
	ADD_PROPERTY(PropertyInfo(Variant::INT, "friction"), "set_friction", "get_friction");
	ADD_PROPERTY(PropertyInfo(Variant::INT, "bounce"), "set_bounce", "get_bounce");
}

void SGRigidBody2D::apply_impulse(const Ref<SGFixedVector2>& p_offset, const Ref<SGFixedVector2>& p_impulse) {
	get_internal()->apply_impulse(p_offset->get_internal(), p_impulse->get_internal());
}

Array SGRigidBody2D::get_contacts() const {
	SGWorld2DInternal* world = SGPhysics2DServer::get_singleton()->get_default_world_internal();
	std::list<SGContactInternal> contacts = world->get_contacts(fixed::ONE, get_internal());
	Array result;
	for (SGContactInternal contact : contacts) {
		result.append(Ref<SGFixedVector2>(memnew(SGFixedVector2(contact.rA + contact.A->get_transform().get_origin()))));
		result.append(Ref<SGFixedVector2>(memnew(SGFixedVector2(contact.rB + contact.B->get_transform().get_origin()))));
	}
	return result;
}

void SGRigidBody2D::set_mass(int64_t p_mass) {
	get_internal()->set_mass(fixed(p_mass));
}

int64_t SGRigidBody2D::get_mass() {
	return get_internal()->get_mass().value;
}

void SGRigidBody2D::set_inertia(int64_t p_inertia) {
	get_internal()->set_inertia(fixed(p_inertia));
}

int64_t SGRigidBody2D::get_inertia() {
	return get_internal()->get_inertia().value;
}


void SGRigidBody2D::set_damp(int64_t p_damp) {
	get_internal()->set_damp(fixed(p_damp));
}

int64_t SGRigidBody2D::get_damp() {
	return get_internal()->get_damp().value;
}

void SGRigidBody2D::set_angular_damp(int64_t p_angular_damp) {
	get_internal()->set_angular_damp(fixed(p_angular_damp));
}

int64_t SGRigidBody2D::get_angular_damp() {
	return get_internal()->get_angular_damp().value;
}

void SGRigidBody2D::set_gravity_scale(int64_t p_gravity_scale) {
	get_internal()->set_gravity_scale(fixed(p_gravity_scale));
}

int64_t SGRigidBody2D::get_gravity_scale() {
	return get_internal()->get_gravity_scale().value;
}

void SGRigidBody2D::set_velocity(const Ref<SGFixedVector2>& p_velocity) {
	get_internal()->set_velocity(p_velocity->get_internal());
}

Ref<SGFixedVector2> SGRigidBody2D::get_velocity() {
	return Ref<SGFixedVector2>(memnew(SGFixedVector2(get_internal()->get_velocity())));
}

void SGRigidBody2D::set_angular_velocity(int64_t p_angular_velocity) {
	get_internal()->set_angular_velocity(fixed(p_angular_velocity));
}

int64_t SGRigidBody2D::get_angular_velocity() {
	return get_internal()->get_angular_velocity().value;
}

void SGRigidBody2D::set_friction(int64_t p_friction) {
	get_internal()->set_friction(fixed(p_friction));
}

int64_t SGRigidBody2D::get_friction() {
	return get_internal()->get_friction().value;
}

void SGRigidBody2D::set_bounce(int64_t p_bounce) {
	get_internal()->set_bounce(fixed(p_bounce));
}

int64_t SGRigidBody2D::get_bounce() {
	return get_internal()->get_bounce().value;
}

Dictionary SGRigidBody2D::save_internals() {
	Dictionary d;
	SGFixedVector2Internal velocity = get_internal()->get_velocity();
	d["velocity_x"] = velocity.x.value;
	d["velocity_y"] = velocity.y.value;
	d["angular_velocity"] = get_angular_velocity();
	SGFixedVector2Internal bias_velocity = get_internal()->get_bias_velocity();
	d["bias_velocity_x"] = bias_velocity.x.value;
	d["bias_velocity_y"] = bias_velocity.y.value;
	d["bias_angular_velocity"] = get_internal()->get_bias_angular_velocity().value;
	return d;
}

void SGRigidBody2D::load_internals(Dictionary d) {
	get_internal()->set_velocity(SGFixedVector2Internal(fixed(d["velocity_x"]), fixed(d["velocity_y"])));
	set_angular_velocity(d["angular_velocity"]);
	get_internal()->set_bias_velocity(SGFixedVector2Internal(fixed(d["bias_velocity_x"]), fixed(d["bias_velocity_y"])));
	get_internal()->set_bias_angular_velocity(fixed(d["bias_angular_velocity"]));
}

SGRigidBody2D::SGRigidBody2D()
	: SGPhysicsBody2D(SGPhysics2DServer::get_singleton()->collision_object_create(SGPhysics2DServer::OBJECT_BODY, SGPhysics2DServer::BODY_RIGID))
{
}

SGRigidBody2D::~SGRigidBody2D() {
}